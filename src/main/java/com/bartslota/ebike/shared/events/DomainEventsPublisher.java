package com.bartslota.ebike.shared.events;

/**
 * @author bslota on 08/01/2021
 */
public interface DomainEventsPublisher {

    void publish(DomainEvent domainEvent);
}
