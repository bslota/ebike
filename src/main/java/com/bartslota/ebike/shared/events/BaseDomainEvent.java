package com.bartslota.ebike.shared.events;

import java.time.Instant;
import java.util.UUID;

/**
 * @author bslota on 08/01/2021
 */
public abstract class BaseDomainEvent implements DomainEvent {

    private final UUID id;
    private final Instant occurredAt;

    public BaseDomainEvent() {
        this.id = UUID.randomUUID();
        this.occurredAt = Instant.now(); //Clock should be used here
    }

    @Override
    public UUID id() {
        return id;
    }

    @Override
    public Instant occurredAt() {
        return occurredAt;
    }
}
