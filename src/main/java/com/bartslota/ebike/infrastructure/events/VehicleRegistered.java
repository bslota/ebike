package com.bartslota.ebike.infrastructure.events;

import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class VehicleRegistered extends BaseDomainEvent {

    private static final String TYPE = "VehicleRegistered";

    private final VehicleId vehicleId;

    private VehicleRegistered(VehicleId vehicleId) {
        super();
        this.vehicleId = vehicleId;
    }

    public static VehicleRegistered from(VehicleId vehicleId) {
        return new VehicleRegistered(vehicleId);
    }

    @Override
    public String type() {
        return TYPE;
    }

    public VehicleId vehicleId() {
        return vehicleId;
    }
}
