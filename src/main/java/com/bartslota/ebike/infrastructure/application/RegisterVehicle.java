package com.bartslota.ebike.infrastructure.application;

import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;

/**
 * @author bslota on 05/01/2021
 */
public class RegisterVehicle {

    private final VehicleId vehicleId;

    RegisterVehicle(VehicleId vehicleId) {
        this.vehicleId = vehicleId;
    }

    public static RegisterVehicle of(VehicleId vehicleId) {
        return new RegisterVehicle(vehicleId);
    }
}
