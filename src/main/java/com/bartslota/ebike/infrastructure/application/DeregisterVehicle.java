package com.bartslota.ebike.infrastructure.application;

import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;

/**
 * @author bslota on 05/01/2021
 */
public class DeregisterVehicle {

    private final VehicleId vehicleId;

    DeregisterVehicle(VehicleId vehicleId) {
        this.vehicleId = vehicleId;
    }

    public static DeregisterVehicle of(VehicleId vehicleId) {
        return new DeregisterVehicle(vehicleId);
    }
}
