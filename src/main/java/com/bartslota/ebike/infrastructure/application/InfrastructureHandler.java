package com.bartslota.ebike.infrastructure.application;

/**
 * @author bslota on 05/01/2021
 */
public class InfrastructureHandler {

    public void handle(LockVehicle command) {
        //some communication with the station
    }

    public void handle(RegisterVehicle command) {
    }

    public void handle(DeregisterVehicle command) {
    }
}
