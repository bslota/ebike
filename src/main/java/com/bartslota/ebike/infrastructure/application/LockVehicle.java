package com.bartslota.ebike.infrastructure.application;

import com.bartslota.ebike.infrastructure.publishedlanguage.UserId;
import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;

/**
 * @author bslota on 05/01/2021
 */
public class LockVehicle {

    private final VehicleId vehicleId;
    private final UserId userId;

    LockVehicle(VehicleId vehicleId, UserId userId) {
        this.vehicleId = vehicleId;
        this.userId = userId;
    }

    public static LockVehicle of(VehicleId vehicleId, UserId ownerId) {
        return new LockVehicle(vehicleId, ownerId);
    }
}
