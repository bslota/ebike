package com.bartslota.ebike.availability.events;

import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class AssetRegistered extends BaseDomainEvent {

    private static final String TYPE = "AssetRegistered";

    private final AssetId assetId;

    private AssetRegistered(AssetId assetId) {
        super();
        this.assetId = assetId;
    }

    public static AssetRegistered from(AssetId assetId) {
        return new AssetRegistered(assetId);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
