package com.bartslota.ebike.availability.events;

import java.time.LocalDateTime;

import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.availability.domain.OwnerId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLocked extends BaseDomainEvent {

    private static final String TYPE = "AssetLocked";

    private final AssetId assetId;
    private final OwnerId ownerId;
    private final LocalDateTime validUntil;

    private AssetLocked(AssetId assetId, OwnerId ownerId, LocalDateTime from) {
        super();
        this.assetId = assetId;
        this.ownerId = ownerId;
        this.validUntil = from;
    }

    public static AssetLocked from(AssetId assetId, OwnerId ownerId, LocalDateTime from) {
        return new AssetLocked(assetId, ownerId, from);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
