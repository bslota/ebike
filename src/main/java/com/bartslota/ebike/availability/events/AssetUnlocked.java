package com.bartslota.ebike.availability.events;

import java.time.LocalDateTime;

import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.availability.domain.OwnerId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class AssetUnlocked extends BaseDomainEvent {

    private static final String TYPE = "AssetUnlocked";

    private final AssetId assetId;
    private final OwnerId ownerId;
    private final LocalDateTime unlockedAt;

    private AssetUnlocked(AssetId assetId, OwnerId ownerId, LocalDateTime unlockedAt) {
        super();
        this.assetId = assetId;
        this.ownerId = ownerId;
        this.unlockedAt = unlockedAt;
    }

    public static AssetUnlocked from(AssetId assetId, OwnerId ownerId, LocalDateTime from) {
        return new AssetUnlocked(assetId, ownerId, from);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
