package com.bartslota.ebike.catalogue.application;

/**
 * @author bslota on 14/02/2021
 */
class RegisterBike {

    private String bikeId;
    private String model;

    RegisterBike(String bikeId, String model) {
        this.bikeId = bikeId;
        this.model = model;
    }

    String bikeId() {
        return bikeId;
    }

    String model() {
        return model;
    }
}
