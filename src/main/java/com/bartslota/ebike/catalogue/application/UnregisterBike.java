package com.bartslota.ebike.catalogue.application;

/**
 * @author bslota on 14/02/2021
 */
class UnregisterBike {

    private String bikeId;

    UnregisterBike(String bikeId) {
        this.bikeId = bikeId;
    }

    String bikeId() {
        return bikeId;
    }
}
