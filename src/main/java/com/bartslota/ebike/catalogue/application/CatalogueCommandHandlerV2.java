package com.bartslota.ebike.catalogue.application;

import org.springframework.transaction.annotation.Transactional;

import com.bartslota.ebike.availability.application.AvailabilityService;
import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.catalogue.domain.Bike;
import com.bartslota.ebike.catalogue.domain.BikeId;
import com.bartslota.ebike.catalogue.domain.Catalogue;
import com.bartslota.ebike.catalogue.domain.Model;
import com.bartslota.ebike.catalogue.events.BikeRegistered;
import com.bartslota.ebike.catalogue.events.BikeUnregistered;
import com.bartslota.ebike.infrastructure.application.InfrastructureHandler;
import com.bartslota.ebike.infrastructure.application.RegisterVehicle;
import com.bartslota.ebike.infrastructure.events.VehicleRegistered;
import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;
import com.bartslota.ebike.shared.events.DomainEventsPublisher;

/**
 * @author bslota on 14/02/2021
 */
@Transactional
//IoC
//addresses availability issues from CatalogueCommandHandler
// drawback: operations might want to see the status of registration (e.g. availability service rejects the registration)
class CatalogueCommandHandlerV2 {

    private final Catalogue catalogue;
    private final DomainEventsPublisher domainEventsPublisher;

    CatalogueCommandHandlerV2(Catalogue catalogue, DomainEventsPublisher domainEventsPublisher) {
        this.catalogue = catalogue;
        this.domainEventsPublisher = domainEventsPublisher;
    }

    public void handle(RegisterBike command) {
        BikeId id = BikeId.of(command.bikeId());
        Model model = Model.valueOf(command.model());
        Bike bike = Bike.registeredFor(id, model);
        catalogue.saveIfMissing(bike);
        domainEventsPublisher.publish(BikeRegistered.from(id, model));
    }

    public void handle(UnregisterBike command) {
        BikeId id = BikeId.of(command.bikeId());
        Bike bike = catalogue.findBy(id).orElseThrow(Catalogue.BikeNotFound::new);
        bike.deregister();
        //why don't we delete the bike?
        //- we want to keep track of historical reservations (read models, or event stores could enable us to remove data)
        catalogue.update(bike);
        domainEventsPublisher.publish(BikeUnregistered.from(id)); //maybe not necessary here - might be useful for search enginge
    }

    class CatalogueEventHandler {

        private final AvailabilityService availabilityService;
        private final InfrastructureHandler infrastructureHandler;

        CatalogueEventHandler(AvailabilityService availabilityService, InfrastructureHandler infrastructureHandler) {
            this.availabilityService = availabilityService;
            this.infrastructureHandler = infrastructureHandler;
        }

        void on(BikeRegistered event) {
            infrastructureHandler.handle(RegisterVehicle.of(VehicleId.of(event.bikeId().asString()))); //using command handler
            //and here we wait for the event from infrastructure handler (if we sent a command)
            //or we emit another event in the catalogue - but if we have 1000 stations to be updated, it's better to wait for an event anyway
        }

        void on(VehicleRegistered event) {
            availabilityService.withdraw(AssetId.of(event.vehicleId().asString())); //using app service
        }
    }

}
