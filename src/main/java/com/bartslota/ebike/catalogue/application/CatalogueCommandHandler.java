package com.bartslota.ebike.catalogue.application;

import org.springframework.transaction.annotation.Transactional;

import com.bartslota.ebike.availability.application.AvailabilityService;
import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.catalogue.domain.Bike;
import com.bartslota.ebike.catalogue.domain.BikeId;
import com.bartslota.ebike.catalogue.domain.Catalogue;
import com.bartslota.ebike.catalogue.domain.Model;
import com.bartslota.ebike.catalogue.events.BikeRegistered;
import com.bartslota.ebike.catalogue.events.BikeUnregistered;
import com.bartslota.ebike.infrastructure.application.DeregisterVehicle;
import com.bartslota.ebike.infrastructure.application.InfrastructureHandler;
import com.bartslota.ebike.infrastructure.application.RegisterVehicle;
import com.bartslota.ebike.infrastructure.publishedlanguage.VehicleId;
import com.bartslota.ebike.shared.events.DomainEventsPublisher;

/**
 * @author bslota on 14/02/2021
 */
@Transactional
class CatalogueCommandHandler {

    private final Catalogue catalogue;
    private final InfrastructureHandler infrastructureHandler;
    private final DomainEventsPublisher domainEventsPublisher;

    CatalogueCommandHandler(Catalogue catalogue, AvailabilityService availabilityService, InfrastructureHandler infrastructureHandler, DomainEventsPublisher domainEventsPublisher) {
        this.catalogue = catalogue;
        this.infrastructureHandler = infrastructureHandler;
        this.domainEventsPublisher = domainEventsPublisher;
    }

    //what about availability?
    public void handle(RegisterBike command) {
        BikeId id = BikeId.of(command.bikeId());
        Model model = Model.valueOf(command.model());
        Bike bike = Bike.registeredFor(id, model);
        catalogue.saveIfMissing(bike);
        infrastructureHandler.handle(RegisterVehicle.of(VehicleId.of(id.asString()))); //using command handler
        domainEventsPublisher.publish(BikeRegistered.from(id, model)); //maybe not necessary here - might be useful for search engine
    }

    public void handle(UnregisterBike command) {
        BikeId id = BikeId.of(command.bikeId());
        Bike bike = catalogue.findBy(id).orElseThrow(Catalogue.BikeNotFound::new);
        bike.deregister();
        //why don't we delete the bike?
        //- we want to keep track of historical reservations (read models, or event stores could enable us to remove data)
        catalogue.update(bike);
        //can I remove a bike that is not available?
        //- usually we don't withdraw a bike that is connected to a station
        //- but... we are humans, we should check if it's possible
        infrastructureHandler.handle(DeregisterVehicle.of(VehicleId.of(id.asString()))); //using command handler
        domainEventsPublisher.publish(BikeUnregistered.from(id)); //maybe not necessary here - might be useful for search engine
    }

}
