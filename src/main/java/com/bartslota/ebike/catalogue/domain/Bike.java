package com.bartslota.ebike.catalogue.domain;

import static com.bartslota.ebike.catalogue.domain.Status.REGISTERED;
import static com.bartslota.ebike.catalogue.domain.Status.REMOVED;

/**
 * @author bslota on 14/02/2021
 */
public class Bike {

    private BikeId id;
    private Model model;
    private Status status;

    private Bike(BikeId id, Model model, Status status) {
        this.id = id;
        this.model = model;
        this.status = status;
    }

    public static Bike registeredFor(BikeId id, Model model) {
        return new Bike(id, model, REGISTERED);
    }

    public void deregister() {
        this.status = REMOVED;
    }

    public BikeId id() {
        return id;
    }

    Model model() {
        return model;
    }
}
