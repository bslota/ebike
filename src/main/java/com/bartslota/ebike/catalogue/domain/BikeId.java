package com.bartslota.ebike.catalogue.domain;

import java.util.Objects;

/**
 * @author bslota on 14/02/2021
 */
public class BikeId {

    private final String id;

    private BikeId(String id) {
        this.id = id;
    }

    public static BikeId of(String value) {
        return new BikeId(value);
    }

    public String asString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BikeId)) {
            return false;
        }
        BikeId bikeId = (BikeId) o;
        return Objects.equals(id, bikeId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
