package com.bartslota.ebike.catalogue.domain;

import java.util.Optional;

/**
 * @author bslota on 14/02/2021
 */
public interface Catalogue {

    void saveIfMissing(Bike bike); //insert with duplicate key handling

    Optional<Bike> findBy(BikeId id);

    void update(Bike bike);

    class BikeNotFound extends RuntimeException {

    }
}
