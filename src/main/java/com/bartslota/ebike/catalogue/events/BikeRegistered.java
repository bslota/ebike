package com.bartslota.ebike.catalogue.events;

import com.bartslota.ebike.catalogue.domain.BikeId;
import com.bartslota.ebike.catalogue.domain.Model;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class BikeRegistered extends BaseDomainEvent {

    private static final String TYPE = "BikeRegistered";

    private final BikeId bikeId;
    private final Model model;

    private BikeRegistered(BikeId bikeId, Model model) {
        super();
        this.bikeId = bikeId;
        this.model = model;
    }

    public static BikeRegistered from(BikeId bikeId, Model model) {
        return new BikeRegistered(bikeId, model);
    }

    @Override
    public String type() {
        return TYPE;
    }

    public BikeId bikeId() {
        return bikeId;
    }

    Model model() {
        return model;
    }
}
