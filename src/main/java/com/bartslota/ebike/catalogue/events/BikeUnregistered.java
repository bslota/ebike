package com.bartslota.ebike.catalogue.events;

import com.bartslota.ebike.catalogue.domain.BikeId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class BikeUnregistered extends BaseDomainEvent {

    private static final String TYPE = "BikeUnregistered";

    private final BikeId bikeId;

    private BikeUnregistered(BikeId bikeId) {
        super();
        this.bikeId = bikeId;
    }

    public static BikeUnregistered from(BikeId bikeId) {
        return new BikeUnregistered(bikeId);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
