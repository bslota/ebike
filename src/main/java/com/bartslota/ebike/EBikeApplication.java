package com.bartslota.ebike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EBikeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EBikeApplication.class, args);
    }

}
